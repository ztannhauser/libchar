

char to_lowercase(char c)
{
	return 'A' <= c && c <= 'Z' ? c - 'A' + 'a' : c;
}

