#include <stdbool.h>

bool is_digit(char c)
{
	return ('0' <= c && c <= '9');
};
