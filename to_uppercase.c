
char to_uppercase(char c)
{
	return 'a' <= c && c <= 'z' ? c - 'a' + 'A' : c;
}
