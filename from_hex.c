
#include <stdlib.h>

int from_hex(char c)
{
	int ret;
	switch(c)
	{
		case '0' ... '9': ret = c - '0'; break;
		case 'A' ... 'F': ret = c - 'A' + 10; break;
		case 'a' ... 'f': ret = c - 'a' + 10; break;
		default: abort();
	}
	return ret;
}

