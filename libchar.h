#ifndef libchar_H
#define libchar_H

#include <stdbool.h>

bool is_num(char c);
int from_hex(char c);

#include <stdbool.h>

bool is_digit(char c);

#include <stdbool.h>

bool is_wspace(char c);
bool is_hexdigit(char c);
char to_lowercase(char c);
#endif
