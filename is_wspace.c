#include <stdbool.h>

bool is_wspace(char c)
{
	return (c == ' ') || (c == '\t') || (c == '\n');
};
